package com.kalehase.tic_tac_toe;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {
	
	public int playerTurn = 0;
	public int totalTurns = 0;
	public boolean gameWon = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}
	
	// Plays a turn of the game on button click
	public void playGame(View v)
	{
		if(gameWon)
		{
			resetGame(v);
			return;
		}
		Button b = (Button)v;
		TextView statusMsg;
		statusMsg =(TextView)findViewById(R.id.status_message);
		if(isChoiceValid(v))
		{
			if(playerTurn == 0)
			{
				b.setText("X");
				totalTurns++;
				playerTurn = 1;
				if(isGameOver(v))
					statusMsg.setText("Player X wins!");
				else if(totalTurns == 9)
				{
					statusMsg.setText("The Game is a Tie!");
					gameWon = true;
				}
				else
					statusMsg.setText("Player O's Turn");
			}
			else
			{
				b.setText("O");
				totalTurns++;
				playerTurn = 0;
				if(isGameOver(v))
					statusMsg.setText("Player O wins!");
				else if(totalTurns == 9)
				{
					statusMsg.setText("The Game is a Tie!");
					gameWon = true;
				}
				else
					statusMsg.setText("Player X's Turn");
			}
		}
		else
		{
			statusMsg.setText("Square already chosen!  Choose another.");
		}
	}
	
	// Determines if a button is already marked
	private boolean isChoiceValid(View v)
	{
		boolean validity = true;
		Button b = (Button)v;
	    String buttonText = b.getText().toString();
	    
	    if(buttonText.equals("X") || buttonText.equals("O"))
	    	validity = false;
	    
	    return validity;
	}
	
	private boolean isGameOver(View v)
	{
		if (totalTurns == 9)
			return false;
		
		// Declare variables
		boolean gameOver = false;
		Button b1, b2, b3, b4, b5, b6, b7, b8, b9;
		TextView statusMsg;
		
		// Link XML to Java
		b1 = (Button)findViewById(R.id.button1);
		b2 = (Button)findViewById(R.id.button2);
		b3 = (Button)findViewById(R.id.button3);
		b4 = (Button)findViewById(R.id.button4);
		b5 = (Button)findViewById(R.id.button5);
		b6 = (Button)findViewById(R.id.button6);
		b7 = (Button)findViewById(R.id.button7);
		b8 = (Button)findViewById(R.id.button8);
		b9 = (Button)findViewById(R.id.button9);
		statusMsg =(TextView)findViewById(R.id.status_message);
		
		// Convert button text to strings for comparison
		int b1Str = convertToInt(b1.getText().toString());
		int b2Str = convertToInt(b2.getText().toString());
		int b3Str = convertToInt(b3.getText().toString());
		int b4Str = convertToInt(b4.getText().toString());
		int b5Str = convertToInt(b5.getText().toString());
		int b6Str = convertToInt(b6.getText().toString());
		int b7Str = convertToInt(b7.getText().toString());
		int b8Str = convertToInt(b8.getText().toString());
		int b9Str = convertToInt(b9.getText().toString());
		
		// Account for all possible win scenarios
		if(b1Str == b2Str && b2Str == b3Str && b1Str != 0) // top
		{
				gameOver = true;
				gameWon = true;
		}
		else if(b1Str== b5Str && b5Str == b9Str && b1Str != 0) //down-diagonal
		{
				gameOver = true;
				gameWon = true;
		}
		else if(b4Str == b5Str && b5Str == b6Str && b4Str != 0) // horizontal center
		{
				gameOver = true;
				gameWon = true;
		}
		else if(b7Str == b8Str && b8Str == b9Str && b7Str != 0) // Bottom
		{
				gameOver = true;
				gameWon = true;
		}
		else if(b1Str == b4Str && b4Str == b7Str && b7Str != 0) // left
		{
				gameOver = true;
				gameWon = true;
		}
		else if(b2Str == b5Str && b5Str == b8Str && b2Str != 0) // vertical center
		{
				gameOver = true;
				gameWon = true;
		}
		else if(b3Str == b6Str && b6Str == b9Str && b3Str != 0) // right
		{
				gameOver = true;
				gameWon = true;
		}
		else if(b3Str == b5Str && b5Str == b7Str && b3Str != 0) // up-diagonal
		{
				gameOver = true;
				gameWon = true;
		}
		
		return gameOver;
	}
	
	private int convertToInt(String s)
	{
		int equivalent = 0;
		
		if(s.equals("X"))
			equivalent = 1;
		else if(s.equals("O"))
			equivalent = 2;
		
		return equivalent;
	}
	
	// Resets game
	public void resetGame(View v)
	{
		// Declare variables
		Button b1, b2, b3, b4, b5, b6, b7, b8, b9;
		TextView statusMsg;
		playerTurn = 0;
		totalTurns = 0;
		gameWon = false;
		
		// Link variables to XML IDs
		b1 = (Button)findViewById(R.id.button1);
		b2 = (Button)findViewById(R.id.button2);
		b3 = (Button)findViewById(R.id.button3);
		b4 = (Button)findViewById(R.id.button4);
		b5 = (Button)findViewById(R.id.button5);
		b6 = (Button)findViewById(R.id.button6);
		b7 = (Button)findViewById(R.id.button7);
		b8 = (Button)findViewById(R.id.button8);
		b9 = (Button)findViewById(R.id.button9);
		statusMsg =(TextView)findViewById(R.id.status_message);
		
		// Reset Text to default
		b1.setText(" ");
		b2.setText(" ");
		b3.setText(" ");
		b4.setText(" ");
		b5.setText(" ");
		b6.setText(" ");
		b7.setText(" ");
		b8.setText(" ");
		b9.setText(" ");
		statusMsg.setText("Player X's Turn");
	}
	
	public void checkForReset(View v)
	{
		if(gameWon == true)
			resetGame(v);
	}

}
